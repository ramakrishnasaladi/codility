const binaryGap = (N) => {
    // declare variables
    let result = 0, temp = 0;
    // Tests if our value is an integer 
    // Tests if N is within range
    if (N === parseInt(N, 10) && N >= 1 && N <= 2147483647) {
        // Convert to binary
        const Binary = N.toString(2);
        // loop through to identify the binary gap
        for (let i = 0; i < Binary.length; i++) {
            // if the string contains 1 then we compare temp and result 
            if (Binary[i] == 1) {
                if (temp > result) {
                    result = temp;
                }
                //reset temp
                temp = 0;
            } else {
                // if it is a zero in the string then we will increment the counter
                temp++;
            }
        }
    }

    // default if it doesn't meet the requirements
    return result;
}
